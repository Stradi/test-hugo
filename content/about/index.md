---
layout: page
multilingual: false
---

## À-PROPOS
**Christine Stevens** est une musicienne, thérapeute et auteure qui a fondé le site web Chamanic Drumming. Elle est née et a grandi à Los Angeles et a commencé sa carrière musicale en jouant du tambour chamanique après avoir été introduite au shamanisme par son amie et mentor, le chamane Peruvian. Elle a ensuite étudié avec de nombreux chamanes du monde entier, ce qui lui a permis de développer sa propre approche unique du tambour chamanique. Aujourd'hui, elle enseigne et partage sa passion pour le tambour chamanique avec des gens du monde entier.

